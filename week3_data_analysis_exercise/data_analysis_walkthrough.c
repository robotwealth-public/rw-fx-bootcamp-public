/*
Data analysis exercise walkthrough

Zorro can work with tick data, but there are limitations with the free version (minimum 1-minute bar period)
If you have Zorro v2.12 or greater, you have the chart viewer as a free feature. 

Open your Zorro configuration file and set HistoryFolder = "D:\path-to-tick-data"

*/

function run()
{
	set(PLOTNOW);
	StartDate = 20190401; 
	EndDate = 20190405;   
	History = ".t1";
	BarPeriod = 1;
	
	asset("EUR/USD");
	vars closes = series(priceClose());
	
	// volatility of one-minute returns
	int stddev_period = 60;
	vars returns = series(ROCP(closes, 1));
	vars volatility = series(100*sqrt(Moment(returns, stddev_period, 2)));
	
	plot("volatility", volatility, NEW, BLUE);
	if(between(ltod(ET), 700, 1100))
		plot("#volatility", volatility, 0, RED);
	
	
	
}