void plotSeason(
	int n,		// plotted bar number
	int label,	// bar label
	int season, // season number
	var value,	// profit to plot
	int type)		// cumulative or difference
{
	if(is(INITRUN) || !is(TESTMODE)) return; // [Test] only

	static int lastseason = 0;
	static var value0 = 0;
	if(!(type&PVAL) && season != lastseason) {
		value0 = value;
		lastseason = season;
	} 
	
	plotBar("StdDev",n,0,(value-value0)/4,NEW|DEV|BARS,COLOR_DEV);	
	plotBar("Value",n,label,value-value0,AVG|BARS|LBL2,COLOR_AVG);	
	
	if(type&PDIFF) value0 = value;
}