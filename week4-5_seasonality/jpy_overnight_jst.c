/* OVERNIGHT EFFECT
 
*/
 
// define sessions
int timezone = JST;
int sessionStart = 8;
int sessionEnd = 18;
 
function run()
{
	set(PLOTNOW);
	setf(PlotMode, PL_FINE);
	StartDate = 2009;
	EndDate = 2018;
	BarPeriod = 60;
	MaxShort = 1;
	asset("USD/JPY");
	
	if(lhour(timezone) == sessionStart)
	{
		enterShort();
	}
	if(lhour(timezone) == sessionEnd)
	{
		exitShort();
	}
}