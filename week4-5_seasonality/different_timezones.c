/*
Intraday seasonality 

Long the foreign currency vs USD during US working hours and short outside of US working hours.

*/

// define environment and load rw_tools
#define ACCT_CCY "USD"
#define DENOM_CCY "USD"
#define ASSET_LIST "AssetsDWX-FX-G10"

#include <rw_tools.c>

function run()
{
	setf(PlotMode, PL_ALL+PL_FINE);
	set(LOGFILE+PLOTNOW);
	BarPeriod = 60; 
	BarZone = ET;	
	StartDate = 2009; 
	EndDate   = 2019; 
	LookBack = 1;
	MaxLong = MaxShort = 1;
	MonteCarlo = 0;  // saves time at the end of the simulation
	
	assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST)); 
	
	asset("EUR/USD");
	
	// Define times to trade in tz time
	int timezone = ET;
	int start_hour = 10;
	int end_hour = 16;
	
	// set transaction costs to zero
	Spread = Commission = Slippage = RollLong = RollShort = 0;
	
	// go long foreign currency at start hour
	if(lhour(timezone) == start_hour) 
	{
		enterLong();
	}
	
	// short foreign currency at end hour
	if(lhour(timezone) == end_hour) 
	{
		exitLong();  // explicitly exit the long in case we use the condition below
		
		if(ldow(timezone) < 5) // make sure we don't get into a position on a Friday afternoon
		{
			enterShort();
		}
	}
}