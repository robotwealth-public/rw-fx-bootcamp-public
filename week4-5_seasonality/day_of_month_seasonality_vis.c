#include <profile.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 1998;
  	EndDate = 2018;
  	BarPeriod = 1440;
  	LookBack = 31;
 
  	vars Close = series(priceClose());
  	vars Return = series((Close[0] - Close[1])/Close[1]);
  	
  	plotMonth(Return[0], 1);
}