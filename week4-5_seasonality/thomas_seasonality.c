#define ACCT_CCY "USD"
#define DENOM_CCY "USD"
#define ASSET_LIST "AssetsDWX-FX"

#include <rw_tools.c>

int timezone = ET; // we assume NY time
int sessionStart = 5;
int sessionEnd = 8;

function run()
{
   set(PLOTNOW);
   setf(PlotMode, PL_FINE);
   StartDate = 2009;
   EndDate = 2019;
   BarPeriod = 60;
   BarOffset = 2;
   MaxShort = 24;
  assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST));
  while(asset(loop("EUR/USD", "USD/JPY"))) //, "AUD/USD", "NZD/USD", "GBP/USD", "USD/CAD", "USD/CHF", "NZD/USD")))
  {
	  // asset("EUR/USD");
	  Spread = Commission = Slippage = RollLong = RollShort = 0; // don't get too excited - ZERO slippage, commission and spread
	    if(strmid(Asset, 0, 3) == "USD") // base currency is USD
		{
			if(lhour(timezone) == sessionStart)
			{
				enterLong();
			}
		    if(lhour(timezone) == sessionEnd)
		    {
				exitLong();
		    }
		}
		else
		{
			if(lhour(timezone) == sessionStart)
			{
				enterShort();
			}
		   if(lhour(timezone) == sessionEnd)
		   {
				exitShort();
		   }
			
		}
	   
  }  

}