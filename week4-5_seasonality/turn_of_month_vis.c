// define environment and load rw_tools
#define ACCT_CCY "USD"
#define DENOM_CCY "USD"
#define ASSET_LIST "AssetsDWX-FX-G10"

#include <rw_tools.c>
#include <profile.c>

function run()
{
	set(PLOTNOW);
  	StartDate = 2000;
  	EndDate = 2018;
  	BarPeriod = 1440;
	LookBack = 31;
	MaxLong = MaxShort = 1;
	assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST)); 
 
  	vars Close = series(priceClose());
  	vars Return = series((Close[0] - Close[1])/Close[1]);
  	
  	plotMonth(Return[0], 0);
}