/*
Intraday seasonality 
Long the foreign currency vs USD during US working hours and short outside of US working hours.

USAGE:
1. Define timezone
2. Comment/uncomment #define COSTS to exclude/include costs
3. Define strategy specifications (string asset, int start_hour, int end_hour) and pass to trade_intraday_seasonality_on_hour function

Example usage in run()

*/

// define environment and load rw_tools
#define ACCT_CCY "USD"
#define DENOM_CCY "USD"
#define ASSET_LIST "AssetsDWX-FX-G10"
#include <rw_tools.c>

#define TIMEZONE ET
#define COSTS

void trade_intraday_seasonality_on_hour(string instrument, int start_hour, int end_hour)
{
	algo(strf("on_hour_%d-%d", start_hour, end_hour));
	
	// set transaction costs to zero
#ifndef COSTS
	Spread = Commission = Slippage = RollLong = RollShort = 0;
#endif
	
	asset(instrument);
		
	if(strmid(Asset, 0, 3) == "USD") // base currency is USD
	{
		// go long foreign currency at start hour
		if(lhour(TIMEZONE) == start_hour) 
		{
			enterShort();
		}
		
		// short foreign currency at end hour
		if(lhour(TIMEZONE) == end_hour) 
		{
			exitShort();  // explicitly exit the long in case we use the condition below
			
			if(ldow(TIMEZONE) < 5) // make sure we don't get into a position on a Friday afternoon
			{
				enterLong();
			}
		}
	}
	else
	{
		// go long foreign currency at start hour
		if(lhour(TIMEZONE) == start_hour) 
		{
			enterLong();
		}
		
		// short foreign currency at end hour
		if(lhour(TIMEZONE) == end_hour) 
		{
			exitLong();  // explicitly exit the long in case we use the condition below
			
			if(ldow(TIMEZONE) < 5) // make sure we don't get into a position on a Friday afternoon
			{
				enterShort();
			}
		}
	}
	
}

function run()
{
	setf(PlotMode, PL_ALL+PL_FINE);
	set(PLOTNOW);
	BarPeriod = 60; 
	BarZone = ET;	
	StartDate = 2009; 
	EndDate   = 2019; 
	LookBack = 1;
	MaxLong = MaxShort = 1;
	MonteCarlo = 0;  // saves time at the end of the simulation
	
	assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST)); 
	int i, j;
	
	trade_intraday_seasonality_on_hour("EUR/USD", 10, 14);
	trade_intraday_seasonality_on_hour("EUR/USD", 11, 16);
	trade_intraday_seasonality_on_hour("GBP/USD", 10, 15);
	trade_intraday_seasonality_on_hour("GBP/USD", 11, 16);
	trade_intraday_seasonality_on_hour("USD/CAD", 10, 16);
	trade_intraday_seasonality_on_hour("USD/CAD", 11, 17);
	
}