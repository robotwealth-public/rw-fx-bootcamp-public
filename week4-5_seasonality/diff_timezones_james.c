/*
Intraday seasonality 
Long the foreign currency vs USD during US working hours and short outside of US working hours.

USAGE:
1. Define timezone
2. Comment/uncomment #define COSTS to exclude/include costs
3. Set START, END, ASETLOOP in #define statemnts to control simulation stat and end dates and bar perio.                    
4. In run function, define strategy specifications (string asset, int start_hour, int end_hour) and pass to4 trade_intraday_seasonality_on_hour function
5. To enter on off-hour times that are the same for all assets and algos, set BarPeriod to 60 and #define OFFSET to the number of minutes past the hour to enter.
6. To enter at a given time of the format HHMM, use the function trade_intraday_seasonality_on_time. BUT....make sure BarPeriod is granular enough!!

Example usage in run()

*/

// define environment and load rw_tools
#define ACCT_CCY "USD"
#define DENOM_CCY "USD
#define ASSET_LIST "AssetsDWX-FX-G10"
#include <rw_tools.c>

#define START 2018
#define END 2019
#define ASSETLOOP "EUR/USD", "GBP/USD", "USD/CAD"
#define TIMEZONE ET
// #define COSTS
#define OFFSET 0
#define PERIOD 60

function evaluate()
{
	print(TO_REPORT, "\nInformation Ratio (x100)");
	for(all_algos)
	{
		var algosum=0;
		var algosqsum = 0;
		int algocount;
		var algomean;
		var algosd;
		var algosharpe = 0;
		for(all_trades)
		{
			if(TradeAsset == Asset and TradeAlgo == Algo)
			{
				algosum += TradeProfit;
				algosqsum += TradeProfit*TradeProfit;
				algocount++;
			}
		}
		if(algocount)
		{
			algomean = algosum/algocount;
			algosd = sqrt((algosqsum/algocount) - algomean*algomean);
			algosharpe = 100*algomean/algosd;
			
			print(TO_REPORT, "\n%s-%s %.2f", Asset, Algo, algosharpe);
		}
		
		
	}

}

void trade_intraday_seasonality_on_time(string instrument, int start_time, int end_time)
{
	
	algo(strf("time_%d-%d", start_time, end_time));
	
	// set transaction costs to zero
#ifndef COSTS
	Spread = Commission = Slippage = RollLong = RollShort = 0;
#endif
	
	asset(instrument);
		
	if(strmid(Asset, 0, 3) == "USD") // base currency is USD
	{
		// go long foreign currency at start time
		if(ltod(TIMEZONE) == start_time) 
		{
			enterShort();
		}
		
		// short foreign currency at end time
		if(ltod(TIMEZONE) == end_time) 
		{
			exitShort();  // explicitly exit the long in case we use the condition below
			
			if(ldow(TIMEZONE) < 5) // make sure we don't get into a position on a Friday afternoon
			{
				enterLong();
			}
		}
	}
	else
	{
		// go long foreign currency at start hour
		if(ltod(TIMEZONE) == start_time) 
		{
			enterLong();
		}
		
		// short foreign currency at end hour
		if(ltod(TIMEZONE) == end_time) 
		{
			exitLong();  // explicitly exit the long in case we use the condition below
			
			if(ldow(TIMEZONE) < 5) // make sure we don't get into a position on a Friday afternoon
			{
				enterShort();
			}
		}
	}
	
}


void trade_intraday_seasonality_on_hour(string instrument, int start_hour, int end_hour)
{
	algo(strf("on_hour_%d-%d", start_hour, end_hour));
	
	// set transaction costs to zero
#ifndef COSTS
	Spread = Commission = Slippage = RollLong = RollShort = 0;
#endif
	
	asset(instrument);
		
	if(strmid(Asset, 0, 3) == "USD") // base currency is USD
	{
		// go long foreign currency at start hour
		if(lhour(TIMEZONE) == start_hour) 
		{
			enterShort();
		}
		
		// short foreign currency at end hour
		if(lhour(TIMEZONE) == end_hour) 
		{
			exitShort();  // explicitly exit the long in case we use the condition below
			
			if(ldow(TIMEZONE) < 5) // make sure we don't get into a position on a Friday afternoon
			{
				enterLong();
			}
		}
	}
	else
	{
		// go long foreign currency at start hour
		if(lhour(TIMEZONE) == start_hour) 
		{
			enterLong();
		}
		
		// short foreign currency at end hour
		if(lhour(TIMEZONE) == end_hour) 
		{
			exitLong();  // explicitly exit the long in case we use the condition below
			
			if(ldow(TIMEZONE) < 5) // make sure we don't get into a position on a Friday afternoon
			{
				enterShort();
			}
		}
	}
	
}

function run()
{
	setf(PlotMode, PL_ALL+PL_FINE);
	set(PLOTNOW);
	BarPeriod = PERIOD; 
	BarZone = ET;	
	BarOffset = OFFSET;
	StartDate = START; 
	EndDate   = END; 
	LookBack = 1;
	MaxLong = MaxShort = 1;
	MonteCarlo = 0;  // saves time at the end of the simulation
	
	assetList(strf("%s\\%s.csv", AL_PATH, ASSET_LIST)); 
	int i, j;
	
	// USAGE
	
	// Single Specification
	// ---------------------
	// trade_intraday_seasonality_on_hour("EUR/USD", 11, 17);
	
	// Loop through start hours for single instrument
	// ----------------------------------------------------
	// for(i=9; i<12; i++)
	// {
		// trade_intraday_seasonality_on_hour("EUR/USD", i, 17);

	// }
	
	// Loop through start hour, end hour for multiple instruments
	// ------------------------------------------------------------
	while(asset(loop(ASSETLOOP)))
	{
		for(i=9; i<12; i++)
			for(j=15; j<19; j++)
				trade_intraday_seasonality_on_hour(Asset, i, j);			
	}
	
	// Specify time of day, not hour
	// ##########################################################
	// 
	// WARNING: Requires appropriately granular BarPeriod.  #####
	// 
	// ##########################################################
	// trade_intraday_seasonality_on_time("EUR/USD", 1105, 1705);
	
}